import React from 'react';
import { TouchableButton, TouchableText } from '../styled';

const ButtonBase = ({ onPress, title, disabled, color }) => {
    return (
        <TouchableButton onPress={() => onPress()} disabled={disabled} color={color}>
            <TouchableText>{title}</TouchableText>
        </TouchableButton>
    );
};

export default ButtonBase;
