import styled from 'styled-components';

export const TouchableButton = styled.TouchableOpacity`
    margin-top: 16px;
    background-color: ${({ disabled, color, theme }) =>
        disabled ? theme.colors.edward : color || theme.colors.plum};
    padding: 12px;
    border-radius: 8px;
    width: 100%;
`;

export const TouchableText = styled.Text`
    color: ${({ theme }) => theme.colors.t0};
    ${({ theme }) => theme.fonts.regular};
    font-size: ${({ theme }) => theme.sizes.s12};
    letter-spacing: 0.02px;
    align-self: center;
`;
