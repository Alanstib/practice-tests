module.exports = {
  "preset": "react-native",
  "setupFilesAfterEnv": [
    "@testing-library/jest-native/extend-expect"
  ],
  "transformIgnorePatterns": [
    "node_modules/(?!(@react-native|react-native)/)"
  ],
  "setupFiles": [
    "./node_modules/react-native-gesture-handler/jestSetup.js"
  ],
  "moduleFileExtensions": [
    "ts",
    "tsx",
    "js",
    "jsx",
    "json",
    "node"
  ],
  "coverageThreshold": {
      "global": {
          "statements": 80,
      },
  },
}